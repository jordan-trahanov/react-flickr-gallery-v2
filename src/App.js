// @flow

import React from 'react'
import { BrowserRouter, Switch, Route } from 'react-router-dom'

import NotFound from './components/NotFound'
import Page from './components/Page'

class App extends React.PureComponent<*> {
  render() {
    return (
      <BrowserRouter basename={process.env.REACT_APP_BASENAME || '/'}>
        <Switch>
          <Route exact path="/" render={() => <Page title="Home" />} />
          <Route path="/cats" render={() => <Page title="Cats" />} />
          <Route path="/dogs" render={() => <Page title="Dogs" />} />
          <Route path="/birds" render={() => <Page title="Birds" />} />
          <Route path="/computers" render={() => <Page title="Computers" />} />
          <Route path="/search" render={() => <Page title="Search" />} />
          <Route component={NotFound} />
        </Switch>
      </BrowserRouter>
    )
  }
}

export default App
