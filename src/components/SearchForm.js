// @flow

import React from 'react'
import styled from '@emotion/styled'

type SearchFormProps = {
  history: { push: (path: string) => void },
  onSearch: (term: string) => void
}

type SearchFormState = {
  searchText: string
}

const SrchForm = styled.form`
  display: flex;
  justify-content: center;
  margin-bottom: 15px;
`

const Input = styled.input`
  width: 210px;
  height: 38px;
  border: 1px solid #542e91;
  padding: 0 10px;
  border-right: transparent;
  vertical-align: top;
  transition: all 0.2s linear;
  outline: none;
  &:hover,
  &:focus {
    border-color: #fddc06;
  }
`

const Submit = styled.button`
  width: 80px;
  height: 38px;
  background: #542e91;
  border: transparent;
  color: #fff;
  cursor: pointer;
  transition: all 0.2s linear;
  vertical-align: top;
  font-size: 16px;
  &:hover {
    background-color: #fddc06;
  }
`

class SearchForm extends React.PureComponent<SearchFormProps, SearchFormState> {
  state = {
    searchText: ''
  }

  constructor({ history }: SearchFormProps) {
    super()

    // Reset to home
    if (undefined !== history) history.push('/')

    this.handleSubmit = this.handleSubmit.bind(this)
  }

  // Update searchText state on input change
  onSearchChange = (evt: SyntheticInputEvent<HTMLInputElement>): void => {
    this.setState({ searchText: evt.currentTarget.value })
  }

  // Form submit
  handleSubmit = (evt: SyntheticEvent<*>): void => {
    evt.preventDefault()
    let path = `/search?q=${this.state.searchText}`
    this.props.onSearch(this.state.searchText)
    evt.currentTarget.reset()
    this.props.history.push(path)
  }

  render() {
    return (
      <SrchForm onSubmit={this.handleSubmit}>
        <Input
          type="search"
          name="search"
          placeholder="Search text..."
          onChange={this.onSearchChange}
          required
        />
        <Submit type="submit">Search</Submit>
      </SrchForm>
    )
  }
}

export default SearchForm
