// @flow

import React from 'react'
import { render } from 'react-testing-library'
import { BrowserRouter, Switch } from 'react-router-dom'
import Page from '../Page'

it('Page renders without photos', () => {
  const { container, getByText } = render(
    <BrowserRouter basename="/">
      <Switch>
        <Page title="Awesome" />
      </Switch>
    </BrowserRouter>
  )
  getByText('Awesome')
  getByText('No Results Found')
})

it('Page renders with proper child tags', () => {
  const { container, getByText } = render(
    <BrowserRouter basename="/">
      <Switch>
        <Page title="Awesome" />
      </Switch>
    </BrowserRouter>
  )
  expect(container.childNodes.length).toEqual(4)
  expect(container.firstChild.nodeName).toEqual('H2')
  expect(container.childNodes[1].tagName).toEqual('DIV')
  expect(container.childNodes[2].tagName).toEqual('NAV')
  expect(container.childNodes[3].tagName).toEqual('DIV')
  expect(container).toMatchSnapshot()
})
