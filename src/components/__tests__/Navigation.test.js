// @flow

import React from 'react'
import { render } from 'react-testing-library'
import { BrowserRouter, Switch } from 'react-router-dom'
import Navigation from '../Navigation'

it('Navigation renders', () => {
  const { container, getByText } = render(
    <BrowserRouter basename="/">
      <Switch>
        <Navigation />
      </Switch>
    </BrowserRouter>
  )

  getByText('Home')
  expect(container.childNodes.length).toEqual(1)
  expect(container.firstChild.firstChild.nodeName).toEqual('A')
  expect(container.firstChild).toMatchSnapshot()
})
