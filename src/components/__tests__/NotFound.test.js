// @flow

import React from 'react'
import { render } from 'react-testing-library'
import NotFound from '../NotFound'

it('"No Results Found" renders', () => {
  const { container, getByText } = render(<NotFound />)
  getByText('No Results Found')
  expect(container.firstChild.firstChild.nodeName).toEqual('H3')
  expect(container.firstChild).toMatchSnapshot()
})
