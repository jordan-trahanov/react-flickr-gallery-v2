// @flow

import React from 'react'
import { render } from 'react-testing-library'
import SearchForm from '../SearchForm'

const history = { push: () => {} }
const performSearch = () => {}

it('Search renders', () => {
  const { container, getByText } = render(
    <SearchForm history={history} onSearch={performSearch} />
  )
  getByText('Search')
  expect(container.firstChild.childNodes.length).toEqual(2)
  expect(container.firstChild.nodeName).toEqual('FORM')
  expect(container.firstChild.firstChild.nodeName).toEqual('INPUT')
  expect(container.firstChild.childNodes[1].tagName).toEqual('BUTTON')
  expect(container.firstChild.childNodes[1].textContent).toEqual('Search')
  expect(container.firstChild).toMatchSnapshot()
})
