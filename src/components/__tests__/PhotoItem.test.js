// @flow

import React from 'react'
import { render } from 'react-testing-library'
import PhotoItem from '../PhotoItem'

const photoItem = {
  imgId: '40707085933',
  imgSrc: 'https://farm66.staticflickr.com/65535/40707085933_deff646cbc.jpg',
  imgTitle: 'Awesome title',
  photo: {
    url: 'https://www.flickr.com/photos/8257780@N08/40707085933/',
    authorName: 'awesomeuser',
    description: 'Awesome description',
    authorUrl: 'https://www.flickr.com/people/8257780@N08/',
    tags: 'awesome, stuff'
  }
}

it('Photo Item renders correctly', () => {
  const { container, getByText } = render(<PhotoItem {...photoItem} />)
  getByText('Awesome title')
  expect(container.firstChild).toMatchSnapshot()
})
