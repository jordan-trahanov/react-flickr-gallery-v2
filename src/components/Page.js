// @flow

import React from 'react'
import { withRouter } from 'react-router-dom'
import styled from '@emotion/styled'

import withSearchResults from '../containers/withSearchResults'
import PhotoList from './PhotoList'
import Navigation from './Navigation'
import SearchForm from './SearchForm'

type PageProps = {
  title: string,
  photos: {
    farm: number,
    id: string,
    owner: string,
    secret: string,
    server: string,
    title: string
  }[],
  history: { push: (path: string) => void },
  performSearch: (term: string) => void,
  perPage: (evt: SyntheticEvent<*>) => void
}

const PageTitle = styled.h2`
  margin: 80px 0 40px;
  text-align: center;
`

const SearchWrap = styled.div`
  margin: 0 0 30px;
  display: flex;
  justify-content: center;
  flex-wrap: wrap;
`

const Selectlabel = styled.label`
  margin: 0 40px;
  display: flex;
  align-items: center;
  height: 38px;
  margin-bottom: 15px;
`
const Select = styled.select`
  height: 100%;
  background: none;
  border: 1px solid #542e91;
  padding-left: 10px;
  margin-left: 10px;
  font-size: 14px;
  transition: all 0.2s linear;
  outline: none;
  &:hover {
    border-color: #fddc06;
  }
`
const SelectOption = styled.option``

class Page extends React.PureComponent<PageProps> {
  render() {
    const { title, history, photos } = this.props

    return (
      <React.Fragment>
        {title === 'Home' ? (
          <PageTitle>Most Recent Photos</PageTitle>
        ) : (
          <PageTitle>{title}</PageTitle>
        )}
        <SearchWrap>
          <SearchForm history={history} onSearch={this.props.performSearch} />
          <Selectlabel>
            Photos per page
            <Select name="perPage" onChange={this.props.perPage}>
              {[12, 24, 48, 96, 192].map(value => (
                <SelectOption key={value} value={value}>
                  {value}
                </SelectOption>
              ))}
            </Select>
          </Selectlabel>
        </SearchWrap>
        <Navigation />
        <PhotoList photos={photos} />
      </React.Fragment>
    )
  }
}

export default withSearchResults(withRouter(Page))
