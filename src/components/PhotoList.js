// @flow

import React from 'react'
import styled from '@emotion/styled'

import NotFound from './NotFound'
import PhotoItem from './PhotoItem'

type PhotoListProps = {
  photos: {
    farm: number,
    id: string,
    owner: string,
    secret: string,
    server: string,
    title: string
  }[]
}

const PhotoGallery = styled.div`
  display: flex;
  justify-content: center;
  flex-wrap: wrap;
  flex-grow: 0;
  flex-shrink: 1;
  font-size: 14px;
`

const PhotoList = ({ photos }: PhotoListProps) => {
  let photoItems = photos.map(photo => {
    const url = `https://farm${photo.farm}.staticflickr.com/${photo.server}/${
      photo.id
    }_${photo.secret}.jpg`
    return (
      <PhotoItem
        key={photo.id}
        imgId={photo.id}
        imgSrc={url}
        imgTitle={photo.title}
      />
    )
  })

  return photos.length > 0 ? (
    <PhotoGallery>{photoItems}</PhotoGallery>
  ) : (
    <NotFound />
  )
}

export default PhotoList
