// @flow

import React from 'react'
import styled from '@emotion/styled'

const NotFoundWrap = styled.div``

const NotFound = () => (
  <NotFoundWrap className="not-found">
    <h3>No Results Found</h3>
    <p>Your search did not return any results. Please try again.</p>
  </NotFoundWrap>
)

export default NotFound
