// @flow

import React from 'react'
import styled from '@emotion/styled'

import withImageDetails from '../containers/withImageDetails'

type PhotoItemProps = {
  imgId: string,
  imgSrc: string,
  imgTitle: string,
  photo: {
    url: string,
    authorName: string,
    description: string,
    authorUrl: string,
    tags: string
  }
}

type PhotoItemState = {}

const PhotoCard = styled.div`
  width: 280px;
  margin: 0 10px 30px;
  padding: 7px 7px 15px;
  margin-bottom: 40px;
  text-align: left;
  border: 1px solid #542e91;
  color: #777;
  transition: all 0.2s linear;
  &:hover {
    border-color: #fddc06;
  }
`

const ImageLink = styled.a`
  height: 220px;
  display: block;
`

const Image = styled.img`
  display: block;
  object-position: center;
  object-fit: cover;
  width: 100%;
  height: 100%;
  max-width: 100%;
  vertical-align: middle;
`

const Caption = styled.div`
  font-size: 12px;
  font-weight: bold;
  padding-bottom: 3px;
  margin-top: 7px;
`

const LinkStyles = `
display: inline-block;
vertical-align: top;
  color: #542e91;
  text-decoration: none;
  transition: all 0.2s linear;
  &:hover {
    color: #fddc06;
  }
`

const InlineLink = `
  width: 100%;
  overflow: hidden;
  white-space: nowrap;
  font-size: 13px;
  font-weight: normal;
  ${LinkStyles};
`
const InlineBlock = `
  display: inline-block;
  width: 100%;
  vertical-align: top;
  border-bottom: 1px solid #ddd;
  margin-bottom: 3px;
  padding-bottom: 7px;
  &:last-of-type {
    border-bottom: none;
    margin-bottom: 0;
    padding-bottom: 0;
  }
`

const TitleWrap = styled.div`
  ${InlineBlock};
`
const TitleLink = styled.a`
  ${InlineLink};
  text-overflow: ellipsis;
`

const AuthorWrap = styled.div`
  ${InlineBlock};
`
const AuthorLink = styled.a`
  ${InlineLink};
`

const DescriptionWrap = styled.div`
  ${InlineBlock};
`

const Description = styled.div`
  max-height: 160px;
  overflow: hidden;
  a {
    ${LinkStyles};
  }
`

const TagsWrap = styled.div`
  ${InlineBlock};
`
const Tags = styled.div`
  max-height: 80px;
  overflow: hidden;
`

class PhotoItem extends React.PureComponent<PhotoItemProps, PhotoItemState> {
  render() {
    const { photo, imgSrc, imgTitle: title } = this.props
    const imageTitle = title !== '' ? title : 'Untitled'

    return (
      <PhotoCard>
        <ImageLink href={photo.url} target="_blank" rel="noreferrer noopener">
          <Image src={imgSrc} alt="" />
        </ImageLink>
        <TitleWrap>
          <Caption>Title</Caption>
          <TitleLink href={photo.url} target="_blank" rel="noreferrer noopener">
            {imageTitle}
          </TitleLink>
        </TitleWrap>
        <AuthorWrap>
          <Caption>Author</Caption>
          <AuthorLink
            href={photo.authorUrl}
            target="_blank"
            rel="noreferrer noopener"
          >
            {photo.authorName}
          </AuthorLink>
        </AuthorWrap>
        <DescriptionWrap>
          <Caption>Description</Caption>
          <Description
            dangerouslySetInnerHTML={{ __html: photo.description }}
          />
        </DescriptionWrap>
        <TagsWrap>
          <Caption>Tags</Caption>
          <Tags>{photo.tags}</Tags>
        </TagsWrap>
      </PhotoCard>
    )
  }
}

export default withImageDetails(PhotoItem)
