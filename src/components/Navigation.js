// @flow

import React from 'react'
import { NavLink } from 'react-router-dom'
import styled from '@emotion/styled'

type NavItemType = {
  url: string,
  name: string
}

const Nav = styled.nav`
  margin: 0 0 40px;
  font-size: 16px;
  display: flex;
  justify-content: center;
  flex-wrap: wrap;
`

const NavLinkStyled = styled(NavLink)`
  background: #542e91;
  color: #fff;
  padding: 0 15px;
  border-radius: 3px;
  height: 32px;
  line-height: 32px;
  margin: 0 10px 10px;
  transition: all 0.2s linear;
  text-decoration: none;
  &:hover {
    background-color: #fddc06;
  }
`

const NavItem = ({ url, name }: NavItemType) => (
  <NavLinkStyled to={url}>{name}</NavLinkStyled>
)

const navCategories = ['Cats', 'Dogs', 'Birds', 'Computers']

const Navigation = () => (
  <Nav className="main-nav">
    <NavItem url="/" name="Home" />
    {navCategories.map(nav => (
      <NavItem key={nav} url={`/${nav.toLowerCase()}`} name={nav} />
    ))}
  </Nav>
)

export default Navigation
