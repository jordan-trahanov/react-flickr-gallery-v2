// @flow

type MapImageDataProps = {
  urls: {
    url: {
      _content: string
    }[]
  },
  owner: {
    username: string,
    nsid: string
  },
  description: {
    _content: string
  },
  tags: {
    tag: {
      _content: string
    }[]
  }
}

const mapPhotoTags = (tags: Array<*>) => {
  let photoTags = []

  if (tags.length) tags.forEach(tag => photoTags.push(tag._content))
  else return '-'

  return photoTags.join(', ')
}

const mapImageData = ({
  urls,
  owner,
  description,
  tags
}: MapImageDataProps) => {
  return {
    photo: {
      url: urls.url[0]._content,
      authorName: owner.username,
      description: description._content !== '' ? description._content : '-',
      authorUrl: `https://www.flickr.com/people/${owner.nsid}/`,
      tags: mapPhotoTags(tags.tag)
    },
    loading: false,
    error: {
      status: false
    }
  }
}

export default mapImageData
