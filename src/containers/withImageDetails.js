// @flow

import React, { type ComponentType } from 'react'
import axios from 'axios'

import mapImageData from './mappers'

const SERVICE = process.env.REACT_APP_SERVICE || ''
const APIKEY = process.env.REACT_APP_APIKEY || ''

const AXIOSGET = `${SERVICE}?api_key=${APIKEY}&format=json&nojsoncallback=1`

type ImageDetailsProps = {
  imgId: string,
  imgSrc: string,
  imgTitle: string
}

type ImageDetailsState = {
  photo: {
    url: string,
    authorName: string,
    description: string,
    authorUrl: string,
    tags: string
  },
  loading: boolean,
  error: {
    status: boolean,
    message?: string | null
  }
}

const withImageDetails = (C: ComponentType<*>) =>
  class extends React.PureComponent<ImageDetailsProps, ImageDetailsState> {
    _isMounted = false

    state = {
      photo: {
        url: '#',
        authorName: '',
        description: 'No description provided',
        authorUrl: `#`,
        tags: ''
      },
      loading: true,
      error: {
        status: true,
        message: null
      }
    }

    componentDidMount() {
      this._isMounted = true

      this.fetchPhotoInfo(this.props.imgId)
    }

    componentDidUpdate(prevProps: ImageDetailsProps) {
      if (prevProps.imgId !== this.props.imgId)
        this.fetchPhotoInfo(this.props.imgId)
    }

    componentWillUnmount() {
      this._isMounted = false
    }

    fetchPhotoInfo = (id: string) => {
      axios
        .get(`${AXIOSGET}?&method=flickr.photos.getInfo&photo_id=${id}`)
        .then(result => {
          if (this._isMounted) this.setState(mapImageData(result.data.photo))
        })
        .catch(error => {
          this.setState({
            error: {
              status: true,
              message: 'Fetching data from Flickr failed: ' + error.message
            }
          })
        })
    }

    render() {
      return <C {...this.props} photo={this.state.photo} />
    }
  }

export default withImageDetails
