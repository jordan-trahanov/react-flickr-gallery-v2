// @flow

import React from 'react'
import axios from 'axios'
import type { ComponentType } from 'react'

const SERVICE = process.env.REACT_APP_SERVICE || ''
const APIKEY = process.env.REACT_APP_APIKEY || ''
const AXIOSGET = `${SERVICE}?api_key=${APIKEY}&format=json&nojsoncallback=1`

type SearchResultsProps = {
  title: string
}

type SearchResultsState = {
  photos: {
    farm: number,
    id: string,
    secret: string,
    server: string,
    title: string
  }[],
  perPage: string
}

const withSearchResults = (C: ComponentType<*>) =>
  class extends React.Component<SearchResultsProps, SearchResultsState> {
    _isMounted = false

    state = {
      photos: [],
      perPage: '12'
    }

    componentDidMount() {
      this._isMounted = true

      this.updatePage()
    }

    componentDidUpdate(prevProps: SearchResultsProps) {
      if (prevProps.title === this.props.title) return

      this.updatePage()
    }

    componentWillUnmount() {
      this._isMounted = false
    }

    getRecentPhotos = () => {
      const method = 'flickr.photos.getRecent'
      const perPage = this.state.perPage

      axios
        .get(`${AXIOSGET}&method=${method}&per_page=${perPage}}`)
        .then(result => {
          if (this._isMounted) {
            this.setState(prevState => {
              return {
                photos: result.data.photos.photo,
                perPage: this.state.perPage
              }
            })
          }
        })
        .catch(error => {
          console.log(
            'Error occured while fetching data from Flickr: ' + error.message
          )
        })
    }

    performSearch = (term: string) => {
      const method = 'flickr.photos.search'
      const perPage = this.state.perPage

      axios
        .get(
          `${AXIOSGET}&method=${method}&per_page=${perPage}&safe_search=1&text=${term}}`
        )
        .then(result => {
          if (this._isMounted)
            this.setState({ photos: result.data.photos.photo })
        })
        .catch(error => {
          console.log(
            'Error occured while fetching data from Flickr: ' + error.message
          )
        })
    }

    changePerPage = (evt: SyntheticInputEvent<EventTarget>): void => {
      if (this.state.perPage !== evt.target.value)
        this.setState({ perPage: evt.target.value }, this.updatePage)
    }

    updatePage = () => {
      if (this.props.title === 'Home') this.getRecentPhotos()
      else this.performSearch(this.props.title)
    }

    render() {
      return (
        <C
          {...this.props}
          photos={this.state.photos}
          getRecentPhotos={this.getRecentPhotos}
          performSearch={this.performSearch}
          perPage={this.changePerPage}
        />
      )
    }
  }

export default withSearchResults
